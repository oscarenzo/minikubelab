# MinikubeLab

Before to start please run this command:

    vagrant plugin install vagrant-vbguest

Then:

    vagrant up

Then you can check the virtualbox guest addition status running:

    vagrant vbguest --status
    [master] GuestAdditions 6.1.32 running --- OK.

Then start the cluster with 2 nodes:

    minikube start --nodes 2 -p mykubeproject

Or just one node:

    minikube start -p mykubeproject
