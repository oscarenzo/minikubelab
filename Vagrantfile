Vagrant.configure("2") do |config|

    config.vm.define "master" do |master|
        master.vm.hostname = "minikube"
        master.vm.box = "enzzito/mykubuntu2204-gui"
        # master.vm.box = "gusztavvargadr/ubuntu-desktop"
        master.vm.box_check_update = false

        # disable vbox guest addition auto-update
        master.vbguest.auto_update = false

        master.vm.provision "shell", inline: <<-SHELL
            apt -y update
            apt -y install vim wget git conntrack
            apt -y remove docker docker-engine docker.io containerd runc
            apt -y install ca-certificates curl gnupg lsb-release
            curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
            echo \
            "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
            $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null
            apt -y update
            apt -y install docker-ce docker-ce-cli containerd.io docker-compose-plugin
            usermod -aG docker vagrant
            echo "Installing kubernetes tools"
            curl -o /usr/bin/kubectl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
            curl -o /usr/bin/minikube -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
            wget https://get.helm.sh/helm-v3.9.0-linux-amd64.tar.gz https://github.com/bitnami-labs/sealed-secrets/releases/download/v0.17.5/kubeseal-0.17.5-linux-amd64.tar.gz
            tar xzf helm-v3.9.0-linux-amd64.tar.gz && tar xzf kubeseal-0.17.5-linux-amd64.tar.gz
            install -m 755 kubeseal /usr/bin/ && install -m 755 linux-amd64/helm /usr/bin/
            chmod -v +x /usr/bin/kubectl /usr/bin/minikube
            echo -e "192.168.49.2\tminikube-browser" >> /etc/hosts
            echo "¡¡Now you can run => minikube start -p mykubeproject [minikube dashboard --port=38778 --url=false]!!"
        SHELL

        master.vm.network "private_network", ip: "192.168.56.4"

        master.vm.provider "virtualbox" do |vb|
            # Display the VirtualBox GUI when booting the machine
            vb.gui = true

            # Customize the amount of memory on the VM:
            vb.memory = "4096"
            vb.customize ["modifyvm", :id, "--vram", "128"]
            vb.customize ["modifyvm", :id, "--graphicscontroller", "vmsvga"]
            vb.customize ['modifyvm', :id, '--clipboard-mode', 'bidirectional']
            vb.customize ['modifyvm', :id, '--draganddrop', 'bidirectional']
        end
    end

    config.vm.synced_folder "./", "/vagrant", disabled: false

end
